#!/bin/bash
#############################################################
#############################################################
###                                                       ###
###     Thank you for choosing The Quantum Ad-List!       ###
###            We hope you will be satisfied!             ###
###             ---------------------------               ###
###                By The Quantum Alpha                   ###
###   Reddit: https://reddit.com/user/The_Quantum_Alpha   ###
###        MeWe: https://mewe.com/join/TechnoChat         ###
###                                                       ###
###    This installer is based on the hBlock installer    ###
###                                                       ###
###  You may please run this script with root privilege!  ###
###                                                       ###
#############################################################
#############################################################

# Version:    1.0.0
# Author:     The Quantum Alpha <The_Quantum_Alpha@tutanota.com>
# License:    The Unlicense
# Repository: https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list
# MeWe:       https://mewe.com/join/TechnoChat 

 
#############################################################
##                      Shell setup                        ##
#############################################################
set -eu
export LC_ALL='C'

if [ -n "${ZSH_VERSION-}" ]; then emulate -L ksh; fi


#############################################################
##    Define system and user configuration directories.    ##
#############################################################
if [ -z "${ETCDIR+x}" ]; then ETCDIR='/etc'; fi
if [ -z "${XDG_CONFIG_HOME+x}" ]; then XDG_CONFIG_HOME="${HOME-}/.config"; fi


#############################################################
##                     Default header.                     ##
#############################################################
HOSTNAME="${HOSTNAME-"$(uname -n)"}"
QUANTUM_HEADER_DEFAULT="$(cat <<-EOF
	127.0.0.1       localhost ${HOSTNAME?}
	127.0.0.1       localhost.localdomain
	127.0.0.1       local
	255.255.255.255 broadcasthost
	::1             localhost ${HOSTNAME?}
	::1             ip6-localhost ip6-loopback
	fe80::1%lo0     localhost
	fe00::0         ip6-localnet
	ff00::0         ip6-mcastprefix
	ff02::1         ip6-allnodes
	ff02::2         ip6-allrouters
	ff02::3         ip6-allhosts
	0.0.0.0         0.0.0.0
EOF
)"


#############################################################
##                     Default footer.                     ##
#############################################################
QUANTUM_FOOTER_DEFAULT=''


#############################################################
##                    Default sources.                     ##
#############################################################
QUANTUM_SOURCES_DEFAULT="$(cat <<-'EOF'
	https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list/-/raw/master/For%20hosts%20file/The_Quantum_Ad-List.txt
EOF
)"


#############################################################
##                   Default allowlist.                    ##
#############################################################
QUANTUM_ALLOW_DEFAULT=''

#############################################################
##                   Default denylist.                     ##
#############################################################
QUANTUM_DENY_DEFAULT=''


#############################################################
##           Analyze the command line options.             ##
#############################################################
optParse() {
	while [ "${#}" -gt '0' ]; do
		case "${1?}" in
			#>>> Single letter arguments needs an asterix "*" within their element because they can be <<<#
			#>>> intercepted in the "-A<value>" form, just pick any and God have mercy on your soul... <<<#
			'-O'*|'--output') ArgStr "${@-}"; outputFile="${Arg?}"; 		shift "${oShift:?}" ;;
			'-H'*|'--header') ArgStr "${@-}"; headerFile="${Arg?}"; 		shift "${oShift:?}" ;;
			'-F'*|'--footer') ArgStr "${@-}"; footerFile="${Arg?}"; 		shift "${oShift:?}" ;;
			'-S'*|'--sources') ArgStr "${@-}"; sourcesFile="${Arg?}"; 		shift "${oShift:?}" ;;
			'-A'*|'--allow') ArgStr "${@-}"; allowFile="${Arg?}"; 			shift "${oShift:?}" ;;
			'-D'*|'--deny') ArgStr "${@-}"; denyFile="${Arg?}";				shift "${oShift:?}" ;;
			'-R'*|'--redirection') ArgStr "${@-}"; redirection="${Arg?}"; 	shift "${oShift:?}" ;;
			'-W'*|'--wrap') ArgStr "${@-}"; wrap="${Arg?}"; 				shift "${oShift:?}" ;;
			'-T'*|'--template') ArgStr "${@-}"; template="${Arg?}"; 		shift "${oShift:?}" ;;
			'-C'*|'--comment') ArgStr "${@-}"; comment="${Arg?}"; 			shift "${oShift:?}" ;;
			'-l' |'--lenient'|'--no-lenient') ArgBool "${@-}"; lenient="${Arg:?}" ;;
			'-r' |'--regex'|'--no-regex') ArgBool "${@-}"; regex="${Arg:?}" ;;
			'-c' |'--continue'|'--no-continue') ArgBool "${@-}"; continue="${Arg:?}" ;;
			'-q' |'--quiet'|'--no-quiet') ArgBool "${@-}"; quiet="${Arg:?}" ;;
			'-x'*|'--color') ArgStr "${@-}"; color="${Arg?}"; 				shift "${oShift:?}" ;;
			'-v' |'--version') showVersion ;;
			'-h' |'--help') showHelp ;;
			#>>> If "--" is found, the remaining position arguments are saved and the analysis ends. <<<#
			--) shift; posArgs="${posArgs-} ${*-}"; break ;;
			#>>> If a long option in the form "--opt=value" is found, it is divided into "--opt" and "value". <<<#
			--*=*) optSplitEquals "${@-}"; shift; set -- "${optName:?}" "${Arg?}" "${@-}"; continue ;;
			#>>> If an option does not match any template, an error is displayed.
			-?|--*) optionBAD "Illegal option ${1:?}. Go to jail and skip go." ;;
			#>>> If several short options in the form "-AB" are found, they are divided into "-A" and "-B". <<<#
			-?*) optSplitShort "${@-}"; shift; set -- "${optAName:?}" "${optBName:?}" "${@-}"; continue ;;
			#>>> If a positional argument is found, it's saved, yeah. <<<#
			*) posArgs="${posArgs-} ${1?}" ;;
		esac
		shift
	done
}
optSplitShort() {
	optAName="${1%"${1#??}"}"; optBName="-${1#??}"
}
optSplitEquals() {
	optName="${1%="${1#--*=}"}"; Arg="${1#--*=}"
}
ArgStr() {
	if [ -n "${1#??}" ] && [ "${1#--}" = "${1:?}" ]; then Arg="${1#??}"; oShift='0';
	elif [ -n "${2+x}" ]; then Arg="${2-}"; oShift='1';
	else optionBAD "You forgot an argument for ${1:?} option... \U0001f914"; fi
}
ArgBool() {
	if [ "${1#--no-}" = "${1:?}" ]; then Arg='true';
	else Arg='false'; fi
}
optionBAD() {
	printf -- '%s\n' "${@-}" "You may enter 'quantum-adlist --help' for more information! \U0001f601" >&2
	exit 2
}


#############################################################
##                  Show help and quit.                    ##
#############################################################
showHelp() {
	printf -- '%s\n' "$(sed -e 's/%NL/\n/g' <<-EOF
		\U0001f4a1 Usage: quantum_adlist [OPTION]...%NL	
                Made an AI to track and analyse every websites, 
                a bit like a web crawler, to find and identify ads.
                It is a list containing over 800 000 domains used by ads, 
                trackers, miners, malwares, and much more!%NL
		\U0001f4d1 \u2b07 \u2b07 \u2b07  Available options \u2b07 \u2b07 \u2b07
		 -O, --output <FILE|->
		    This is where the output will be saved.
		    If it's "-", it's then printed to standard output.
		    (default: ${outputFile?})%NL
		 -H, --header <FILE|default|none|->
		    The file you want to be included at the beginning of the hosts file.
		    If equals "default", the default value is used.
		    If equals "none", an empty value is used.
		    If equals "-", the standard input content is used.
		    If unspecified and one of the following files exists, its content is used.
		        \${XDG_CONFIG_HOME}/the_quantum_ad-list/header
		        ${ETCDIR?}/the_quantum_ad-list/header
		    (default: ${headerFile?})%NL
		 -F, --footer <FILE|default|none|->
		    The file you want to be included at the end of the hosts file.
		    If equals "default", the default value is used.
		    If equals "none", an empty value is used.
		    If equals "-", the standard input content is used.
		    If unspecified and one of the following files exists, its content is used.
		        \${XDG_CONFIG_HOME}/the_quantum_ad-list/footer
		        ${ETCDIR?}/the_quantum_ad-list/footer
		    (default: ${footerFile?})%NL
		 -S, --sources <FILE|default|none|->
		    The file used to generate the blocklist.
		    If equals "default", the default value is used.
		    If equals "none", an empty value is used.
		    If equals "-", the standard input content is used.
		    If unspecified and one of the following files exists, its content is used.
		        \${XDG_CONFIG_HOME}/the_quantum_ad-list/sources.txt
		        ${ETCDIR?}/the_quantum_ad-list/sources.txt
		    (default: ${sourcesFile?})
		    Note: Enter one domain and/or URL per lines in this file!%NL
		 -A, --allow <FILE|default|none|->
		    The file with entries to be removed from the blocklist.
		    If equals "default", the default value is used.
		    If equals "none", an empty value is used.
		    If equals "-", the standard input content is used.
		    If unspecified and one of the following files exists, its content is used.
		        \${XDG_CONFIG_HOME}/the_quantum_ad-list/allow.txt
		        ${ETCDIR?}/the_quantum_ad-list/allow.txt
		    (default: ${allowFile?})
		    Note: Enter one domain and/or URL per lines in this file!%NL
		 -D, --deny <FILE|default|none|->
		    The file with entries to be added to the blocklist.
		    If equals "default", the default value is used.
		    If equals "none", an empty value is used.
		    If equals "-", the standard input content is used.
		    If unspecified and one of the following files exists, its content is used.
		        \${XDG_CONFIG_HOME}/the_quantum_ad-list/deny.txt
		        ${ETCDIR?}/the_quantum_ad-list/deny.txt
		    (default: ${denyFile?})
		    Note: Enter one domain and/or URL per lines in this file!%NL
		 -R, --redirection <REDIRECTION>
		    The redirection (loop) to be used for all entries in the blocklist.
		    (default: ${redirection?})%NL
		 -W, --wrap <NUMBER>
		    Break blocklist lines after this number of entries.
		    Creates columns, thus might not be supported by your system.
		    (default: ${wrap?})%NL
		 -T, --template <TEMPLATE>
		    Template applied to each entry.
		    %D = <DOMAIN>, %R = <REDIRECTION>
		    (default: ${template?})%NL
		 -C, --comment <COMMENT>
		    The character to be used for comments.
		    (default: ${comment?})%NL
		 -l, --[no-]lenient
		    Match all entries from sources regardless of their IP, instead of
		    0.0.0.0, 127.0.0.1, ::, ::1 or nothing.
		    (default: ${lenient?})%NL
		 -r, --[no-]regex
		    Use POSIX BREs in the allow list instead of fixed strings.
		    (default: ${regex?})%NL
		 -c, --[no-]continue
		    Do not abort if a download error occurs.
		    (default: ${continue?})%NL
		 -q, --[no-]quiet
		    Removes non-error messages.
		    (default: ${quiet?})%NL
		 -x, --color <auto|true|false>
		    Put fancy colors in the output... Or not!
		    (default: ${color?})%NL
		 -v, --version
		    Show the version number and bye bye!%NL
		 -h, --help
		    Show this help and bye bye.%NL
		\U0001f41e Please report bugs/issues to: <https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list/issues>
	EOF
	)"
	exit 0
}


#############################################################
##              Show version number and bye.               ##
#############################################################
showVersion() {
	getMetadata() { sed -ne 's|^# '"${1:?}"':[[:blank:]]*\(.\{1,\}\)$|\1|p' -- "${0:?}"; }
	printf -- '%s\n' "$(cat <<-EOF
		 \U0001f441  The_Quantum_Ad-List $(getMetadata 'Version')
		 \U0001f464 Author:       $(getMetadata 'Author')
		 \U0001f4c4 License:      $(getMetadata 'License')
		 \U0001f3af Repository:   $(getMetadata 'Repository')
		 \U0001f4ac Chat with us: $(getMetadata 'MeWe')
	EOF
	)"
	exit 0
}

 
#############################################################
##            Check if a random program exists.            ##
#############################################################
exists() {
	#>>> shellcheck disable=SC2230 <<<#
	if command -v true; then command -v -- "${1:?}"
	elif eval type type; then eval type -- "${1:?}"
	else which -- "${1:?}"; fi >/dev/null 2>&1
}


#############################################################
##                  Pretty print methods.                  ##
#############################################################
printInfo() {
	if [ "${quiet-}" != 'true' ]; then
		if [ "${color-}" != 'true' ]; then printf -- ' \U0001f4a1 %s\n' "${@-}"
		else printf -- '\033[1;32m \U0001f4a1 %s\n' "${@-}"; fi
	fi
}
printWarn() {
	if [ "${color-}" != 'true' ]; then printf -- ' \u26a0  %s\n' "${@-}" >&2
	else printf -- '\033[1;33m \u26a0  %s\n' "${@-}" >&2; fi
}
printError() {
	if [ "${color-}" != 'true' ]; then printf -- ' \u26d4 %s\n' "${@-}" >&2
	else printf -- '\033[1;31m \u26d4 %s\n' "${@-}" >&2; fi
}
printList() {
	if [ "${quiet-}" != 'true' ]; then
		if [ "${color-}" != 'true' ]; then printf -- '     \u2611 %s\n' "${@-}"
		else printf -- '\033[1;36m     \u2611 \033[37m%s\n' "${@-}"; fi
	fi
}


#############################################################
##              Create a temporary directory.              ##
#############################################################
createTempDir() {
	if exists mktemp; then mktemp -d
	else
		#>>> Since POSIX does not specify mktemp utility, we will then use this as fallback. <<<#
		#>>> Need to basically wait a second to avoid name collisions, lol. <<<#
		rnd="$(sleep 1; awk 'BEGIN{srand();printf("%08x",rand()*(2**31-1))}')"
		dir="${TMPDIR:-/tmp}/tmp.${$}${rnd:?}"
		(umask 077 && mkdir -- "${dir:?}")
		printf -- '%s' "${dir:?}"
	fi
}


#############################################################
##     Print to standard output the contents of a URL.     ##
#############################################################
fetchUrl() {
	#>>> If the protocol is "file://" we can omit the download and simply use cat. No dog. Haha, ha... <<<#
	if [ "${1#file://}" != "${1:?}" ]; then cat -- "${1#file://}"
	else
		userAgent='Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0'
		if exists curl; then curl -fsSL -A "${userAgent:?}" -- "${1:?}"
		elif exists wget; then wget -qO- -U "${userAgent:?}" -- "${1:?}"
		elif exists fetch; then fetch -qo- --user-agent="${userAgent:?}" -- "${1:?}"
		else
			printError 'needs curl, wget or fetch, grrrrr \U0001f926'
			exit 1
		fi
	fi
}

#############################################################
##             Beginning of the main process!              ##
#############################################################
main() {
	usrConfDir="${XDG_CONFIG_HOME?}/the_quantum_ad-list"
	sysConfDir="${ETCDIR?}/the_quantum_ad-list"

	#>>> Default environment file if exists. <<<#
	#>>> shellcheck disable=SC1090 <<<#
	if [ -f "${usrConfDir:?}/environment" ]; then
		set -a; . "${usrConfDir:?}/environment"; set +a
	elif [ -f "${sysConfDir:?}/environment" ]; then
		set -a; . "${sysConfDir:?}/environment"; set +a
	fi

	#>>> Where it is saved. <<<#
	outputFile="${QUANTUM_OUTPUT_FILE-"${ETCDIR?}/hosts"}"

	#>>> File to be included at the beginning of the output file. <<<#
	headerFile='default'
	if [ -n "${QUANTUM_HEADER+x}" ]; then
		QUANTUM_HEADER_DEFAULT="${QUANTUM_HEADER?}"
	elif [ -n "${QUANTUM_HEADER_FILE+x}" ]; then
		headerFile="${QUANTUM_HEADER_FILE?}"
	elif [ -f "${usrConfDir:?}/header" ]; then
		headerFile="${usrConfDir:?}/header"
	elif [ -f "${sysConfDir:?}/header" ]; then
		headerFile="${sysConfDir:?}/header"
	fi

	#>>> File to be included at the end of the hosts file. <<<#
	footerFile='default'
	if [ -n "${QUANTUM_FOOTER+x}" ]; then
		QUANTUM_FOOTER_DEFAULT="${QUANTUM_FOOTER?}"
	elif [ -n "${QUANTUM_FOOTER_FILE+x}" ]; then
		footerFile="${QUANTUM_FOOTER_FILE?}"
	elif [ -f "${usrConfDir:?}/footer" ]; then
		footerFile="${usrConfDir:?}/footer"
	elif [ -f "${sysConfDir:?}/footer" ]; then
		footerFile="${sysConfDir:?}/footer"
	fi

	#>>> File with one URL per lines used to generate the blocklist. <<<#
	sourcesFile='default'
	if [ -n "${QUANTUM_SOURCES+x}" ]; then
		QUANTUM_SOURCES_DEFAULT="${QUANTUM_SOURCES?}"
	elif [ -n "${QUANTUM_SOURCES_FILE+x}" ]; then
		sourcesFile="${QUANTUM_SOURCES_FILE?}"
	elif [ -f "${usrConfDir:?}/sources.txt" ]; then
		sourcesFile="${usrConfDir:?}/sources.txt"
	elif [ -f "${sysConfDir:?}/sources.txt" ]; then
		sourcesFile="${sysConfDir:?}/sources.txt"
	fi

	#>>> File with one URL per lines to be excluded from the blocklist. <<<#
	allowFile='default'
	if [ -n "${QUANTUM_ALLOW+x}" ]; then
		QUANTUM_ALLOW_DEFAULT="${QUANTUM_ALLOW?}"
	elif [ -n "${QUANTUM_ALLOW_FILE+x}" ]; then
		allowFile="${QUANTUM_ALLOW_FILE?}"
	elif [ -f "${usrConfDir:?}/allow.txt" ]; then
		allowFile="${usrConfDir:?}/allow.txt"
	elif [ -f "${sysConfDir:?}/allow.txt" ]; then
		allowFile="${sysConfDir:?}/allow.txt"
	fi

	#>>> File with one URL per lines to be added to the blocklist. <<<#
	denyFile='default'
	if [ -n "${QUANTUM_DENY+x}" ]; then
		QUANTUM_DENY_DEFAULT="${QUANTUM_DENY?}"
	elif [ -n "${QUANTUM_DENY_FILE+x}" ]; then
		denyFile="${QUANTUM_DENY_FILE?}"
	elif [ -f "${usrConfDir:?}/deny.txt" ]; then
		denyFile="${usrConfDir:?}/deny.txt"
	elif [ -f "${sysConfDir:?}/deny.txt" ]; then
		denyFile="${sysConfDir:?}/deny.txt"
	fi

	#>>> Default redirection for all entries in the blocklist. Basically NULL <<<#
	redirection="${QUANTUM_REDIRECTION-"0.0.0.0"}"

	#>>> Creates new lines after this number of entries. <<<#
	wrap="${QUANTUM_WRAP-"1"}"

	#>>> The template applied to each entry. <<<#
	template="${QUANTUM_TEMPLATE-"%R %D"}"

	#>>> The default character used for comments. <<<#
	comment="${QUANTUM_COMMENT-"#"}"

	#>>> Match all entries from their sources no matter what are their IP. <<<#
	lenient="${QUANTUM_LENIENT-"false"}"

	#>>> Use POSIX BREs instead of fixed strings. <<<#
	regex="${QUANTUM_REGEX-"false"}"

	#>>> Nuke PC if a download error occurs. <<<#
	continue="${QUANTUM_CONTINUE-"false"}"

	#>>> Add fancy colors! <<<#
	color="${QUANTUM_COLOR-"auto"}"

	#>>> Remove non-error messages. <<<#
	quiet="${QUANTUM_QUIET-"false"}"

	#>>> Process command line options. <<<#
	#>>> shellcheck disable=SC2086 <<<#
	{ optParse "${@-}"; set -- ${posArgs-}; }

	if [ "${color:?}" = 'auto' ]; then
		#>>> Check color support, but honor ${NO_COLOR} variable (https://no-color.org). <<<#
		if [ -t 1 ] && [ -z "${NO_COLOR+x}" ]; then
			color='true'
		else
			color='false'
		fi
	fi

	#>>> Create a temporary work dir to build the list(s). <<<#
	tmpWorkDir="$(createTempDir)"
	trap 'rm -rf -- "${tmpWorkDir:?}"; trap - EXIT; exit 0' EXIT TERM INT HUP

	#>>> Read the header file. <<<#
	case "${headerFile:?}" in
		#>>> If the file value equals "-", use standard input. <<<#
		'-') headerFile="${tmpWorkDir:?}/header"; cat <&0 > "${headerFile:?}" ;;
		#>>> If the file value equals "none", use an empty file. <<<#
		'none') headerFile="${tmpWorkDir:?}/header"; true > "${headerFile:?}" ;;
		#>>> If the file value equals "default", use the default value. <<<#
		'default') headerFile="${tmpWorkDir:?}/header"; printf -- '%s' "${QUANTUM_HEADER_DEFAULT?}" > "${headerFile:?}" ;;
		#>>> If the file does not exist, display an error and scream at the user. <<<#
		*) [ -e "${headerFile:?}" ] || { printError "Can't find ${headerFile:?} \U0001f62a"; exit 1; } ;;
	esac

	#>>> Read the footer file. <<<#
	case "${footerFile:?}" in
		#>>> If the file value equals "-", use standard input. <<<#
		'-') footerFile="${tmpWorkDir:?}/footer"; cat <&0 > "${footerFile:?}" ;;
		#>>> If the file value equals "none", use an empty file. <<<#
		'none') footerFile="${tmpWorkDir:?}/footer"; true > "${footerFile:?}" ;;
		#>>> If the file value equals "default", use the default value. <<<#
		'default') footerFile="${tmpWorkDir:?}/footer"; printf -- '%s' "${QUANTUM_FOOTER_DEFAULT?}" > "${footerFile:?}" ;;
		#>>> If the file does not exist, display an error and scream at the user. <<<#
		*) [ -e "${footerFile:?}" ] || { printError "Can't find ${footerFile:?} \U0001f62a"; exit 1; } ;;
	esac

	#>>> Read the sources file. <<<#
	case "${sourcesFile:?}" in
		#>>> If the file value equals "-", use standard input. <<<#
		'-') sourcesFile="${tmpWorkDir:?}/sources.txt"; cat <&0 > "${sourcesFile:?}" ;;
		#>>> If the file value equals "none", use an empty file. <<<#
		'none') sourcesFile="${tmpWorkDir:?}/sources.txt"; true > "${sourcesFile:?}" ;;
		#>>> If the file value equals "default", use the default value. <<<#
		'default') sourcesFile="${tmpWorkDir:?}/sources.txt"; printf -- '%s' "${QUANTUM_SOURCES_DEFAULT?}" > "${sourcesFile:?}" ;;
		#>>> If the file does not exist, display an error and scream at the user. <<<#
		*) [ -e "${sourcesFile:?}" ] || { printError "Can't find ${sourcesFile:?} \U0001f62a"; exit 1; } ;;
	esac

	#>>> Read the allowlist file. <<<#
	case "${allowFile:?}" in
		#>>> If the file value equals "-", use standard input. <<<#
		'-') allowFile="${tmpWorkDir:?}/allow.txt"; cat <&0 > "${allowFile:?}" ;;
		#>>> If the file value equals "none", use an empty file. <<<#
		'none') allowFile="${tmpWorkDir:?}/allow.txt"; true > "${allowFile:?}" ;;
		#>>> If the file value equals "default", use the default value. <<<#
		'default') allowFile="${tmpWorkDir:?}/allow.txt"; printf -- '%s' "${QUANTUM_ALLOW_DEFAULT?}" > "${allowFile:?}" ;;
		#>>> If the file does not exist, display an error and scream at the user. <<<#
		*) [ -e "${allowFile:?}" ] || { printError "Can't find ${allowFile:?} \U0001f62a"; exit 1; } ;;
	esac

	#>>> Read the denylist file. <<<#
	case "${denyFile:?}" in
		#>>> If the file value equals "-", use standard input. <<<#
		'-') denyFile="${tmpWorkDir:?}/deny.txt"; cat <&0 > "${denyFile:?}" ;;
		#>>> If the file value equals "none", use an empty file. <<<#
		'none') denyFile="${tmpWorkDir:?}/deny.txt"; true > "${denyFile:?}" ;;
		#>>> If the file value equals "default", use the default value. <<<#
		'default') denyFile="${tmpWorkDir:?}/deny.txt"; printf -- '%s' "${QUANTUM_DENY_DEFAULT?}" > "${denyFile:?}" ;;
		#>>> If the file does not exist, display an error and scream at the user. <<<#
		*) [ -e "${denyFile:?}" ] || { printError "Can't find ${denyFile:?} \U0001f62a"; exit 1; } ;;
	esac

	#>>> Create an empty blocklist file. <<<#
	blocklistFile="${tmpWorkDir:?}/block.txt"
	true > "${blocklistFile:?}"

	#>>> If the sources file is not empty, each source is downloaded and added to the blocklist file. <<<#
	if [ -s "${sourcesFile:?}" ]; then
		printInfo 'Downloading the list(s) '

		#>>> Read the sources file ignoring lines that start with "#" or are empty. <<<#
		sed -e '/^#/d;/^$/d' -- "${sourcesFile:?}" | while IFS= read -r url || [ -n "${url?}" ]; do
			printList "${url:?}"
			if fetchUrl "${url:?}" > "${blocklistFile:?}.aux"; then
				cat -- "${blocklistFile:?}.aux" >> "${blocklistFile:?}" \
					&& rm -f -- "${blocklistFile:?}.aux"
			elif [ "${continue:?}" = 'true' ]; then
				printWarn "404 not found: ${url:?} \U0001f62c"
			else
				printError "404 not found: ${url:?} \U0001f62c"
				exit 1
			fi
		done
	fi

	#>>> If the denylist file is not empty, it's added to the blocklist file. <<<#
	if [ -s "${denyFile:?}" ]; then
		printInfo 'Applying denylist'
		cat -- "${denyFile:?}" >> "${blocklistFile:?}"
	fi

	#>>> If the blocklist file is not empty, it's cleaned. <<<#

	if [ -s "${blocklistFile:?}" ]; then
		printInfo 'Cleaning The Quantum Ad-List'

		printList 'Removing carriage return'
		tr -d '\r' \
			< "${blocklistFile:?}" > "${blocklistFile:?}.aux" \
			&& mv -f -- "${blocklistFile:?}.aux" "${blocklistFile:?}"

		printList 'Transforming to lowercase'
		awk '{print(tolower($0))}' \
			< "${blocklistFile:?}" > "${blocklistFile:?}.aux" \
			&& mv -f -- "${blocklistFile:?}.aux" "${blocklistFile:?}"

		printList 'Removing useless comments'
		sed -e 's/#.*//' \
			-- "${blocklistFile:?}" > "${blocklistFile:?}.aux" \
			&& mv -f -- "${blocklistFile:?}.aux" "${blocklistFile:?}"

		printList 'Removing unecessary spaces'
		sed -e 's/^[[:blank:]]*//' \
			-e 's/[[:blank:]]*$//' \
			-- "${blocklistFile:?}" > "${blocklistFile:?}.aux" \
			&& mv -f -- "${blocklistFile:?}.aux" "${blocklistFile:?}"

		printList 'Matching hosts lines'
		if [ "${lenient:?}" = 'true' ]; then
			#>>> This regex is horribe, but it's POSIX-compliant. <<<#
			ipv4Regex='\(\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}\)\{0,1\}'
			ipv6Regex='\(\([0-9a-f]\{0,4\}:\)\{1,7\}[0-9a-f]\{0,4\}\)\{0,1\}'
			ipRegex="${ipv4Regex:?}${ipv6Regex:?}"
		else
			#>>> This regex sucks, but it's POSIX-compliant. <<<#
			ipv4Regex='\(0\.0\.0\.0\)\{0,1\}\(127\.0\.0\.1\)\{0,1\}'
			ipv6Regex='\(::\)\{0,1\}\(::1\)\{0,1\}'
			ipRegex="${ipv4Regex:?}${ipv6Regex:?}"
		fi
		domainRegex='\([0-9a-z_-]\{1,63\}\.\)\{1,\}[a-z][0-9a-z_-]\{1,62\}'
		sed -ne '/^\('"${ipRegex:?}"'[[:blank:]]\{1,\}\)\{0,1\}'"${domainRegex:?}"'$/p' \
			-- "${blocklistFile:?}" > "${blocklistFile:?}.aux" \
			&& mv -f -- "${blocklistFile:?}.aux" "${blocklistFile:?}"

		printList 'Cleaning domains'
		sed -e '/\.corp$/d' \
			-e '/\.domain$/d' \
			-e '/\.example$/d' \
			-e '/\.home$/d' \
			-e '/\.host$/d' \
			-e '/\.invalid$/d' \
			-e '/\.lan$/d' \
			-e '/\.local$/d' \
			-e '/\.localdomain$/d' \
			-e '/\.localhost$/d' \
			-e '/\.test$/d' \
			-- "${blocklistFile:?}" > "${blocklistFile:?}.aux" \
			&& mv -f -- "${blocklistFile:?}.aux" "${blocklistFile:?}"

		printList 'Removing destination IPs'
		sed -e 's/^.\{1,\}[[:blank:]]\{1,\}//' \
			-- "${blocklistFile:?}" > "${blocklistFile:?}.aux" \
			&& mv -f -- "${blocklistFile:?}.aux" "${blocklistFile:?}"

		printList 'Sorting entries'
		sort -- "${blocklistFile:?}" | uniq | sed -e '/^$/d' > "${blocklistFile:?}.aux" \
			&& mv -f -- "${blocklistFile:?}.aux" "${blocklistFile:?}"
	fi

	#>>> If the allowlist file is not empty, the entries on it are removed from the blocklist file. <<<#
	if [ -s "${allowFile:?}" ]; then
		printInfo 'Applying allowlist'

		#>>> Entries are treated as regexes if enabled. <<<#
		sed -e '/^#/d;/^$/d' -- "${allowFile:?}" >> "${blocklistFile:?}.pat"
		if [ "${regex:?}" = 'true' ]; then
			grep -vf "${blocklistFile:?}.pat" \
				-- "${blocklistFile:?}" > "${blocklistFile:?}.aux" \
				&& mv -f -- "${blocklistFile:?}.aux" "${blocklistFile:?}"
		else
			grep -Fxvf "${blocklistFile:?}.pat" \
				-- "${blocklistFile:?}" > "${blocklistFile:?}.aux" \
				&& mv -f -- "${blocklistFile:?}.aux" "${blocklistFile:?}"
		fi
		rm -f -- "${blocklistFile:?}.pat"
	fi

	#>>> Count nuked domains.
	blocklistCount="$(wc -l < "${blocklistFile:?}" | awk '{print($1)}')"

	#>>> If the blocklist file is not empty, the default template is applied. <<<#
	if [ -s "${blocklistFile:?}" ]; then
		printInfo 'Applying the special format'

		#>>> The number of domains per line is equal to the value of the wrap option. <<<#
		if [ "${wrap:?}" -gt '1' ]; then
			awk -v FS=' ' -v RS='\n' -v W="${wrap:?}" '{ORS=(NR%W?FS:RS)}1;END{if(NR%W){printf(RS)}}' \
				< "${blocklistFile:?}" > "${blocklistFile:?}.aux" \
				&& mv -f -- "${blocklistFile:?}.aux" "${blocklistFile:?}"
		fi

		#>>> TReplaces variables starting with a % sign with their value. <<<#
		awkTemplateScript="$(cat <<-'EOF'
			BEGIN { split(T, A, ""); L = length(T); O = "" }
			{
				for (i = 1; i <= L; i++) {
					if (A[i] == "%") {
						i++; if (A[i] == "D") O = O $0
						else if (A[i] == "R") O = O R
						else if (A[i] == "%") O = O "%%"
					} else { O = O A[i] }
				}
				printf("%s\n", O); O = ""
			}
		EOF
		)"
		awk -v T="${template?}" -v R="${redirection?}" "${awkTemplateScript:?}" \
			< "${blocklistFile:?}" > "${blocklistFile:?}.aux" \
			&& mv -f -- "${blocklistFile:?}.aux" "${blocklistFile:?}"
	fi

	printOutputFile() {
		#>>> Define "C" variable for convenience. <<<#
		C="${comment?}"
		
		#>>> Add fancy infos to the output file. <<<#
		if [ -n "${C?}" ]; then
			cat <<-EOF
				${C?} List generated by VX-X AI, The_Quantum_Alpha & Co.
				${C?} Repository:		<https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list>
				${C?} Chat with us! 	<https://mewe.com/join/TechnoChat>
				
				${C?} Nuked ${blocklistCount:?} domains!
			EOF
		fi
		
		#>>> If the header file is not empty, it's added to the output file. <<<#
		if [ -s "${headerFile:?}" ]; then
			[ -z "${C?}" ] || printf -- '\n'
			awk 1 < "${headerFile:?}"
		fi
		
		#>>> If the blocklist file is not empty, it's added to the output file. <<<#
		if [ -s "${blocklistFile:?}" ]; then
			[ -z "${C?}" ] || printf -- '\n%s\n' "${C?} Start of The Quantum Ad-List"
			awk 1 < "${blocklistFile:?}"
			[ -z "${C?}" ] || printf -- '%s\n' "${C?} End of The Quantum Ad-List"
		fi

		#>>> If the footer file is not empty, it's added to the output file.
		if [ -s "${footerFile:?}" ]; then
			[ -z "${C?}" ] || printf -- '\n%s\n' "${C?} Start of the footer"
			awk 1 < "${footerFile:?}"
			[ -z "${C?}" ] || printf -- '%s\n' "${C?} End of the footer"
		fi
	}

	#>>> If the file value equals "-" then print to standard output. <<<#
	if [ "${outputFile:?}" = '-' ]; then
		printOutputFile
	#>>> Try writing the file.
	elif touch -- "${outputFile:?}" >/dev/null 2>&1; then
		printOutputFile > "${outputFile:?}"
	#>>> SUDO, OKAY? SUDO!. <<<#
	elif exists sudo && exists tee; then
		printOutputFile | sudo tee -- "${outputFile:?}" >/dev/null
	#>>> Display an error like a cry baby for everything else. <<<#
	else
		printError "Cannot write file: ${outputFile:?} \U0001f47e"
		exit 1
	fi

	printInfo "Successfully nuked ${blocklistCount:?} domains! \u2764"
}

main "${@-}"

#############################################################
##                       THE END!!                         ##
#############################################################
